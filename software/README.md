﻿# Software

- `quanser_original` is the code that can be obtained on a dedicated web page for this product on Quanser website. Simulink files are saved in the format of R2018b release. Although it seems to work fine with newer releases of Simulink, we store it just for archiving purposes here. Do not use it. Use the updated version instead.
- `quanser_updated` is the Quanser code after updating to the later release of Matlab and Simulink, namely R2021a. Just the Simulink file is resaved, no other change was actually needed. With future releases of Matlab and Simulink, the code in this directory might be updated if needed.
