# Quanser Active Mass Dampers on Shake Table

This is a repository with a supporting material (manuals, codes, models, data, ...) for an educational laboratory experiment composed of a two-storey bench-scale model of a tall building with [Active Mass Damper (AMD)](https://www.quanser.com/products/active-mass-damper/) on each storey, and a single-axis vibration platform [Shake Table II from Quanser](https://www.quanser.com/products/shake-table-ii/) emulating earthquake vibrations.

![Full experiment with Shake Table II and 2x AMD1](figures/STII_2xAMD1.png)

## Control systems challenge

## Instructions for running the experiment

1. Double-check that the room around the experiment is free of any obstacles. In particular, check also that the numerous cables are not blocking the motion of the table.

2. Make sure that the the RED BUTTON for stopping the platform by deactivating the amplifiers will be within your reach during the experiments. You will press it if anything goes wrong with the platform to protect it.

3. Switch on the [VoltPAQ-X2 Amplifier](https://www.quanser.com/products/voltpaq-x2-amplifier/) – the switch is on the back side just above the power cable. Check that the gains are correctly switched to 1x on the front panel (just in case somebody changed this setting before you).

4. Switch on the [AMPAQ-PWM amplifier](https://www.quanser.com/products/ampaq-pwm-amplifier/) – the switch is also on the back side. 

5. Connect the [Q8-USB Data Acquisition Device](https://www.quanser.com/products/q8-usb-data-acquisition-device/) to the power (it has no switch, just plug in the connector on the back side). 

6. Push the movable carts (the two AMDs) to the the middle positions on their floors.

7. Use the turning knob on the shake table to set the moving stage to the *home position* – reaching it is indicated by a green LED on the AMPAQ amplifier. In fact, the experiment will not even start if the table is not in the home position.

8. Download the [software/quanser_updated](software/quanser_updated) directory from this repository. 

9. As an alternative to the previous step, go to the [Simulink Courseware](https://quanserinc.app.box.com/s/35ek73astl83h4z42q5r2dp69kk8r74w/folder/26124041063) repository linked at [Shake Table II page](https://www.quanser.com/products/shake-table-ii/). Then go to `Active Mass Dampers` subdirectory, and then within it choose the [Shake Table II and 2xAMD-1](https://quanserinc.app.box.com/s/35ek73astl83h4z42q5r2dp69kk8r74w/folder/82366203243) (the AMD stands for Active Mass Damper and we have two of them) and download the content. After unzipping the downloaded `Shake Table II and 2xAMD-1.zip`, you will have two subdirectories: `Software` and `Documentation`. 

10. Run the `setup_stII_2xamd1.m` within Matlab. It will set all the parameters needed later.

11. Open the `q_2xamd1_stII_q8_usb.slx` Simulink model. This is the "Hello World!" basic example for this experiment. 

12. Note that if you want to record the measured data for later analysis (which you will certainly do), the routes that you are familiar with from purely simulation projects may not work here. The reason is that while using the Quanser QUARC system, you are running the Simulink model in [External Mode](https://www.mathworks.com/help/sldrt/ug/simulink-external-mode.html), which prevents from using some Simulink functionality. In particular, if you [use Simulink scope block(s) for logging the data](https://www.mathworks.com/help/sldrt/ug/set-scope-parameters-for-logging-to-file.html), which is a common route, increasing the parameter called `Limit data points to last` will have no impact on how long the sequence of measurements will be stored in Matlab workspace (but do not forget to tick off the `Log data workspace` option and insert the variable name). If you ignore this issue, you may easily leave the lab assured that you stored a minute or two of experimental data only to discover at home that only the last few seconds have been saved. In order to prevent this, the `Duration` parameter needs to be changed somewhere else. Namely, go to menu `Code`, submenu `External Mode Control Panel`. Click on `Signal & Triggering` and change the `Duration` parameter there. If you need more on this, have a look at [the secion on Data Collection in QUARC manual](https://docs.quanser.com/quarc/documentation/quarc_data_collection.html) but if you are happy with logging to the variables in Matlab workspace through the Simulink scope blocks, you should be fine now. 

13. In the Hardware tab in Simulink, click the green triangle subtitled by "Monitor & Tune". This may take a few seconds and then the experiments starts. 

14. After the experiment starts running, the green triangle should turn into an icon (black square) for stopping the experiment. But if this does not happen (for whatever reason), the fallback solution for stopping the experiment is the "Stop all" option offered through the small Quanser icon at the bottom right panel in Windows.

15. If you fail to get things moving, consult the [TROUBLESHOOTING](TROUBLESHOOTING.md) text.