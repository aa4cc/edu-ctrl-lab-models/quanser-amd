# Troubleshooting 

- Every time the experiment is started (even after interrupting it during one experimental session), the shake table must be in the home position. Turn the knob untill the green LED is on.

- Check that the QUARC configuration block named [HIL Initialize](https://docs.quanser.com/quarc/documentation/hil_initialize_block.html) in the (upper right corner of the) diagram is correctly configured to the `Q8 USB` board (and not `Q8` or anything else). 

- Check that the target is set to `quanser_win64.tlc`(and not `quanser_windows.tlc` or anything else).

- Check that the `Q8 USB` DAQ is powered on – there is no switch, you just have to plug in the cable (the connector is on the back and left). Unfortunately, there is also no LED indicator that the device is powered.

- Check that the `Q8 USB` DAQ is connected to the PC using a USB cable (somebody may have pulled the cable out either from the PC or from the Q8 USB). Apparently not all USB connectors on the PC side are equally usable, you may try another one.

- Unlock the emergency button if it was previously pressed.